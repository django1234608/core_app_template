"""
URL configuration for {{ app_name }} Rest API v1
"""
from django.urls import path
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework_simplejwt.views import TokenObtainPairView,TokenRefreshView

from applications.{{ app_name }}.api.v1 import views

app_name = "applications.{{ app_name }}"

urlpatterns = [
    # SCHEMA
    path('schema/', SpectacularAPIView.as_view(), name='schema'),
    path('schema/swagger-ui/',
         SpectacularSwaggerView.as_view(url_name='applications.{{ app_name }}:schema'),
         name='swagger-ui'),
    # JWT
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]

urlpatterns += [
    # url for the data dictionary & data model
    path("data/dictionary", views.data_dict, name="database-data-dictionary"),
    path("data/model", views.data_model, name="database-data-model"),
    path("status/", views.status, name="health-check"),
    # Add your urls here...
]
