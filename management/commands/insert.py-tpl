import csv
from django.core.management.base import BaseCommand, CommandError
from django.apps import apps
from django.db import transaction
from django.conf import settings


DATA_DIR = getattr(settings, "DATA_DIR")

class Command(BaseCommand):
    help = 'Import data from a CSV file into the specified model'

    def my_get_models(self, options):
        if options['app_name']:
            app_name = "applications." + options['app_name']
        else:
            app_name = "applications"

        # all models
        installed_models = apps.get_models()
        # all models developped by me
        model_names = [
                x.__name__
                for x in installed_models
                if x.__module__.startswith(app_name)
            ]
        return app_name, model_names

    def add_arguments(self, parser):
        parser.add_argument(
            'app_name',
            type=str,
            help='The name of the app to the model exists in'
        )
        parser.add_argument(
            'model_name',
            type=str,
            help='The name of the model to import data into'
        )
        parser.add_argument(
            'csv_file',
            type=str,
            help='The path to the CSV file containing the data'
        )

    def handle(self, *args, **options):
        app_name = options['app_name']
        model_name = options['model_name']
        csv_file = options['csv_file']

        try:
            model = apps.get_model(app_name, model_name)
        except LookupError:
            raise CommandError(f"Model '{model_name}' does not exist in app 'myapp'")

        # TODO: Get model fields
        exclude1 = ('id', 'created_at', 'updated_at')
        # other fields you want to exclude
        exclude2 = ()
        exclude = exclude1 + exclude2
        # model_fields = [field.name for field in model._meta.get_fields() if field.name != 'id']
        model_fields = [field.name for field in model._meta.fields if field.name not in exclude]
        # print(model_fields)

        # if the table is empty...
        is_empty = not model.objects.exists()
        if is_empty:
            print("The table is empty, fill in")
            # Read and validate the CSV file
            with open(DATA_DIR / csv_file, newline='') as f:
                reader = csv.DictReader(f)

                # Check if CSV columns match the model fields
                csv_fields = reader.fieldnames
                for field in model_fields:
                    if field not in csv_fields:
                        raise CommandError(f"CSV file is missing required column '{field}'")

                # Create and save model instances
                instances = []
                # TODO: how to bulk insert only if not exist ?
                for row in reader:
                    instance_data = {field: row[field] for field in model_fields}
                    instances.append(model(**instance_data))

                # Use a transaction to ensure all-or-nothing save
                with transaction.atomic():
                    model.objects.bulk_create(instances)

                self.stdout.write(
                    self.style.SUCCESS(f'Successfully imported data for {model_name} model')
                )
                # TODO: read about load data
                # /howto/initial-data/#provide-data-with-fixtures
        else:
            self.stdout.write(
                self.style.SUCCESS(f'No data imported for {model_name} model, table non empty.')
            )

            print("The table is not empty, skipping")
